#!/bin/bash 





icon_so=~/my_icons/icons 
theme_so=~/my_icons/themes 

icon_des=/usr/share/icons 
theme_des=/usr/share/themes


sudo cp -r $icon_so/BeautyLine $icon_des
sudo cp -r $icon_so/candy-icons $icon_des
sudo cp -r $icon_so/CRUSHED-Limes-Suru $icon_des
sudo cp -r $icon_so/Hey-orange $icon_des
sudo cp -r $icon_so/HighContrast $icon_des
sudo cp -r $icon_so/Infinity-Dark-Icons $icon_des
sudo cp -r $icon_so/Lyra-red $icon_des
sudo cp -r $icon_so/Lyra-red-dark $icon_des
sudo cp -r $icon_so/Mintjaro $icon_des
sudo cp -r $icon_so/RevengeShip $icon_des


# cursor download on 16 Nov 2021
sudo cp -r $icon_so/Afterglow $icon_des
sudo cp -r $icon_so/Polarnight $icon_des


sudo cp -r $theme_so/HighContrast $theme_des
sudo cp -r $theme_so/Raleigh $theme_des


# --- show the done message 

BACK_TITLE="\Z3 Operation has been done successfully"
TITLE="\Z0 Success! $USER please reboot your machine"
MSGBOX="\Z7 Success your icon,theme has been copy to directory \
    \nIcon -> /usr/share/icons \
    \nTheme -> /usr/share/themes \
    \n\Z1 if you're not see your new theme,icon in the list \
    \n\Z1 please logout and login 
    \n\Z7 Thank you $USER for using the script 
    \n\Z6 Farook"

WIDTH=45
HEIGH=17

function goodbye(){
    dialog --clear \
        --colors \
        --backtitle "$BACK_TITLE" \
        --title "$TITLE" \
        --msgbox "$MSGBOX" \
        $HEIGH $WIDTH


}

goodbye
