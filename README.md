# my_icons

> Last update 29 Apr 2021
> my icons is the icon and linux theme  that I use now.


> please note THIS IS THE 200 MB file size it will take time to download!






## my note 

> my OS now is change to `Arch Linux` on both PC and Laptop because I have a 
> feeling like `Arch Linux` is way faster than `Manjaro` and `Debian` before that 
> I have try `Arco Linux` it is one of the good choice linux distribution and 
> very easy ,good looking and very comfortable to use one but I kinda unsatisfy 
> I have too much of `Erik Dubois` and I want pure `Arch` that I have install it 
> myself so that's why I tried to install `Arch` at the first time on my laptop 
> by 10 Nov 2021 and now I use `Arch Linux` full time. 


> by the way `Arch Linux` is not easy for noob but my youtube video is maybe help


<!--[![](http://img.youtube.com/vi/td77N8oauEw/0.jpg)](http://www.youtube.com/watch?v=td77N8oauEw "") -->
[![watch how to install arch linux on laptop](http://img.youtube.com/vi/yD3ub-qQGZY/0.jpg)](http://www.youtube.com/watch?v=yD3ub-qQGZY "")

<!-- https://www.youtube.com/watch?v=yD3ub-qQGZY -->

---

## The icons theme I use now

> the icon theme call "revengeShip"


[regen_blue]:https://i.ibb.co/sRnBBTS/2021-04-28-icons.png

![my icon theme][regen_blue]


---

[cursor_theme]:https://i.ibb.co/J581Fxb/Cursor-themes.png

[icon_theme]:https://i.ibb.co/zGRGnJm/Cursor-themes-1.png


## Cursor Theme


![cursor theme][cursor_theme]




## Icon Theme

![icon theme][icon_theme]
